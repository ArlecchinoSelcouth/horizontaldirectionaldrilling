let position;
let direction;
let bias = -1;
let path = [];

function setup() {
	createCanvas(1000, 1000);
	position = createVector(10, 100);
	direction = p5.Vector.fromAngle(PI/6);

	createButton("Bias").mousePressed(function () {
		bias *= -1;
	});
}

function drill() {

	const angle = 0.01;
	direction.rotate(angle * bias);

	//.copy je bitan jer dodat position 
	//je dodavanje pointera na objekt koji 
	//ce se minjat a ne zapisivanje njegove 
	//vrijednosti
	path.push(position.copy());
	position.add(direction);
}

function draw() {
	drill();

	background(51);
	noStroke();
	rectMode(CORNER);
	fill(139, 69, 19);
	rect(0, 100, width, height-100);
	fill(30, 144, 255);
	arc(width / 2, 100, 500, 200, 0, PI);

	// what are these begin shape dark arts
	beginShape();
	noFill();
	stroke(0);
	strokeWeight(2);
	for(let dot of path) {
		vertex(dot.x, dot.y);
	}
	endShape();

	//draw dot, yes
	stroke(255, 0, 0);
	strokeWeight(4);
	push();
	translate(position.x, position.y);
	rotate(direction.heading() + (PI / 6) * bias);
	line(0, 0, 10, 0);
	pop();
	 
}